	move.w	#2,-(sp)	find the screen
	trap	#14
	addq	#2,sp
	move.l	d0,screen
	lea	blit,a6		pointer to parameter block
	dc.w	$a007		BitBlt
loop    bra     loop

*BitBlit parameter block
blit:	dc.w	$0030		width of source in pixels
	dc.w	$0014		height of source in pixels
	dc.w	$0001		number of planes to blit
	dc.w	$0001		fg colour (logic op index: hi bit)
	dc.w	$0000		bg colour (logic op index: lo bit)
	dc.l	07070707	logic ops for all fg and bg combos
	dc.w	$0000		minimum X: source
	dc.w	$0000		minimum Y: source
	dc.l	slug		source form base address
	dc.w	$0002		byte offset to next word in line
	dc.w	$0006		byte offset to next line in plane
	dc.w	$0002		offset to next plane (in bytes)
	dc.w	$00ff		minimum X: destination
	dc.w	$0064		minimum Y: destination
screen	dc.l	$00000000	destination form base address
	dc.w	$0002		byte offset to next word in line
	dc.w	$0050		byte offset to next line in plane
	dc.w	$0002		offset to next plane (in bytes)
	dc.l	$00000000	address of pattern buffer
	dc.w	$0000		byte offset to next line in pattern
	dc.w	$0000		byte offset tb next plane in pattern
	dc.w	$0000		pattern index mask


* working space
	dc.w	$0000,$0000,$0000,$0000
	dc.w	$0000,$0000,$0000,$0000
	dc.w	$0000,$0000,$0000,$0000


* the image
slug:
* $30 pixels/scanline, $14 scanlines
* monochrome mask (1 plane: background = 0, foreground = 1)
	dc.w	$0000,$0000,$0030,$0000,$0000,$0066,$0000,$0000
	dc.w	$006c,$0000,$0000,$00ce,$0000,$0000,$00cc,$0000
	dc.w	$0000,$0198,$0000,$0000,$03b0,$0000,$0000,$0770
	dc.w	$0000,$0000,$0760,$0000,$0000,$0ee0,$0000,$0000
	dc.w	$7fc0,$0000,$0003,$ffc0,$0000,$003f,$ffc0,$0000
	dc.w	$00ff,$ffe0,$0000,$1fff,$fff0,$01ff,$ffff,$fef0
	dc.w	$0fff,$ffff,$ff70,$1fff,$ffff,$ff80,$ffff,$ffff
	dc.w	$ffe0,$ffff,$ffff,$ffc0
* note: this program changes "constants" in "dc.w"'s; it would
* be better practice to use the "ds.w" directive in the bss section.
