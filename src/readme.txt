===============================================================================
Still have some doubts whether these are transcribed correctly...
-------------------------------------------------------------------------------

POLYFIL2.PRG, CLIPFRME.PRG, PERSPECT.PRG, OTRANW.PRG and ILL_HIDE.PRG

  These should all exit when any key is pressed. The first three do, but
  OTRANW.PRG and ILL_HIDE.PRG freeze instead, needing an emulator reset.

  CORE_03.S and SYSTM_02.S are under suspicion.

  The calls to the terminate system call are in the main program files OTRANW.S
  and ILL_HIDE.S, but are based on testing a register returned from a call to
  scan_keys. scan_keys is in SYSTM_01.S, which seems proven OK - it's also used
  by the three working programs above.

  One new thing done by OTRANW.PRG is page-flipping animation, which means it
  changes the memory addresses mapped to the screen. Key logic for this is in
  SYSTM_02.s. There doesn't seem to be any provision for putting the screen
  back as it was at exit.

  Although this code gets the physical screen address from the XBIOS, it
  calculates a logical screen address from that to use as the second buffer by
  simply subtracting 32K from the physical screen address. It's not clear
  whether there's any guarantee of a separate logical screen buffer (32K was
  a fair overhead on 512K machines, worse on the early 256K machines) and even
  if there is, there's no reason why this subtraction should work. Very likely
  using this second screen buffer memory is trashing something GEM-related.

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

TRNSFRMS.PRG, WRLD_VW.PRG, WRLD_SCN.PRG and EULR_SCN.PRG

  These programs all handle function keys F1..F7, and use F7 to exit. All fail
  to exit, instead freezing and becoming unresponsive until a joystick move is
  made, then crashing out.

  One possible explanation is the same second-screen-buffer issue as OTRANW.PRG
  and ILL_HIDE.PRG. However, the crash-on-joystick-movement aspect suggests
  something more complex may be happening.

  CORE_05.S is under suspicion - SYSTM_04.S seems too simple.

  scan_keys is no longer called directly by the main programs - it's called
  from key_in in CORE_05.S (TRNSFRMS.PRG, WRLD_VW.PRG) and in_key in CORE_07.S
  (WRLD_SCN.PRG and EULR_SCN.PRG).

  From the magazine series, 3D_06.S seems similar to TRNSFRMS.PRG, though with
  no joystick control, and with F3 and F4 doing rotations rather than shears.
  Using F7 to exit seems to crash out to a black screen rather than freezing.

    3D_01.PRG ... Animated POLYFIL2.PRG ... Infinite loop
    3D_02.PRG ... Animated CLIPFRME.PRG ... Infinite loop
    3D_03.PRG ... PERSPECT.PRG ............ Clean exit on any key
    3D_04.PRG ... OTRANW.PRG .............. Clean exit on any key !!!
    3D_05.PRG ... ILL_HIDE.PRG ............ Crash on any key
    3D_06.PRG ... Modified TRNSFRMS.PRG ... Crash on F7

===============================================================================
01-chapter-02\DATA_00.S ..... Tidy
01-chapter-02\POLYFIL0.S .... Tidy
01-chapter-02\POLYFIL1.S .... Tidy
01-chapter-02\PUT_PIXL.S .... Tidy
01-chapter-02\SET_PIXL.S .... Tidy
01-chapter-02\SYSTM_00.S .... Tidy
-------------------------------------------------------------------------------

PUT_PIXL.PRG
  Draw a single pixel using the LINE-A routine

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

SET_PIXL.PRG
  Draw a single pixel using direct access to the screen memory

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

POLYFIL0.PRG
  Draw a rectangle (with a fill pattern) using the LINE-A routine

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

POLYFIL1.PRG
  Draw a rectangle (with solid colour) using direct access to the screen
  memory

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

All of these loop forever, and require a reset to exit them. A cold reset is
advised - at least once, a warm reset left the emulated ST in a non-functional
state.

===============================================================================
02-chapter-04\BSS_00.S ...... Tidy
02-chapter-04\CORE_00.S ..... Tidy
02-chapter-04\POLYFIL2.S .... Tidy
02-chapter-04\SYSTM_00.S .... Unchanged from Chapter 2
02-chapter-04\SYSTM_01.S .... Tidy
-------------------------------------------------------------------------------

POLYFIL2.PRG
  Draw a 5-sided polygon direct to screen memory. Still assumes low
  resolution, so result is a bit odd in medium resolution.

  Press a key to exit - exits cleanly

===============================================================================
03-chapter-05\BSS_01.S ...... Tidy
03-chapter-05\CLIPFRME.S .... Tidy
03-chapter-05\CORE_00.S ..... Unchanged from Chapter 4
03-chapter-05\CORE_01.S ..... Tidy
03-chapter-05\SYSTM_00.S .... Unchanged from Chapter 2
03-chapter-05\SYSTM_01.S .... Unchanged from Chapter 4
-------------------------------------------------------------------------------

CLIPFRME.PRG
  Draw the same 5-sided polygon as for POLYFIL2.PRG (Chapter 5), but
  clipped within a rectangular frame.

  Press a key to exit - exits cleanly

===============================================================================
04-chapter-06\BSS_02.S ...... Tidy
04-chapter-06\CORE_00.S ..... Unchanged from Chapter 4
04-chapter-06\CORE_01.S ..... Unchanged from Chapter 5
04-chapter-06\CORE_02.S ..... Tidy
04-chapter-06\DATA_01.S ..... Tidy
04-chapter-06\DATA_02.S ..... Tidy
04-chapter-06\PERSPECT.S .... Tidy
04-chapter-06\SYSTM_00.S .... Unchanged from Chapter 2
04-chapter-06\SYSTM_01.S .... Unchanged from Chapter 4
-------------------------------------------------------------------------------

PERSPECT.PRG
  Draw an "ST Monolith" in perspective

  Press a key to exit - exits cleanly

===============================================================================
05-chapter-07\BSS_02.S ...... Unchanged from Chapter 6
05-chapter-07\BSS_03.S ...... Tidy
05-chapter-07\CORE_00.S ..... Unchanged from Chapter 4
05-chapter-07\CORE_01.S ..... Unchanged from Chapter 5
05-chapter-07\CORE_02.S ..... Unchanged from Chapter 6
05-chapter-07\CORE_03.S ..... Tidy
05-chapter-07\DATA_01.S ..... Unchanged from Chapter 6
05-chapter-07\DATA_02.S ..... Unchanged from Chapter 6
05-chapter-07\DATA_03.S ..... Tidy
05-chapter-07\OTRANW.S ...... Tidy
05-chapter-07\SYSTM_00.S .... Unchanged from Chapter 2
05-chapter-07\SYSTM_01.S .... Unchanged from Chapter 4
05-chapter-07\SYSTM_02.S .... Tidy
-------------------------------------------------------------------------------

OTRANW.PRG
  Spinning version of the "ST Monolith" from chapter 6

  Press a key to exit - locks machine

===============================================================================
06-chapter-08\JOY_TEST.S .... Tidy
06-chapter-08\KEY_PEEK.S .... Tidy
06-chapter-08\RAMVIEW.S ..... Tidy
06-chapter-08\SYSTM_03.S .... Tidy
-------------------------------------------------------------------------------

JOY_TEST.PRG
  Displays output to indicate joystick inputs. When started, it looks as if
  it may have crashed because the screen doesn't update until there's some
  joystick input to report, and the mouse pointer freezes.

  Infinite loop - no clean exit.

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

KEY_PEEK.PRG
  This program just sits in an infinite loop. It's intended to be run in a
  debugger or monitor, to give a chance to look around using those tools.

  Infinite loop - no clean exit.

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

RAM_VIEW.PRG
  Maps a certain area of RAM to screen memory. Probably even more pointless
  than KEY_PEEK.PRG.

  Infinite loop - no clean exit.

===============================================================================
07-chapter-09\BSS_02.S ...... Unchanged from Chapter 6
07-chapter-09\BSS_03.S ...... Unchanged from Chapter 7
07_chapter-09\BSS_04.S ...... Tidy
07-chapter-09\CORE_00.S ..... Unchanged from Chapter 4
07-chapter-09\CORE_01.S ..... Unchanged from Chapter 5
07-chapter-09\CORE_02.S ..... Unchanged from Chapter 6
07-chapter-09\CORE_03.S ..... Unchanged from Chapter 7
07_chapter-09\CORE_04.S ..... Tidy
07-chapter-09\DATA_01.S ..... Unchanged from Chapter 6
07-chapter-09\DATA_02.S ..... Unchanged from Chapter 6
07-chapter-09\DATA_03.S ..... Unchanged from Chapter 7
07_chapter-09\DATA_04.S ..... Tidy
07_chapter-09\ILL_HIDE.S .... Tidy
07-chapter-09\SYSTM_00.S .... Unchanged from Chapter 2
07-chapter-09\SYSTM_01.S .... Unchanged from Chapter 4
07-chapter-09\SYSTM_02.S .... Unchanged from Chapter 7
-------------------------------------------------------------------------------

ILL_HIDE.PRG
  Another spinning version of the "ST Monolith", this time spinning on a
  different axis than the chapter 7 version. This version has lighting
  effects, and "hidden surfaces" (backface culling). Counting in terms of
  rendered frames, the monolith is hidden the same amount of time as it is
  visible. However, rendering a monolith is obviously slower than not
  rendering it, so running at normal ST speed (8MHz) the timing seems off.

  Press a key to exit - locks machine

===============================================================================
08-chapter-10\BSS_02.S ...... Unchanged from Chapter 6
08-chapter-10\BSS_03.S ...... Unchanged from Chapter 7
08_chapter-10\BSS_04.S ...... Unchanged from Chapter 9
08-chapter-10\BSS_05.S ...... Tidy
08-chapter-10\CORE_00.S ..... Unchanged from Chapter 4
08-chapter-10\CORE_01.S ..... Unchanged from Chapter 5
08-chapter-10\CORE_02.S ..... Unchanged from Chapter 6
08-chapter-10\CORE_03.S ..... Unchanged from Chapter 7
08_chapter-10\CORE_04.S ..... Unchanged from Chapter 9
08-chapter-10\CORE_05.S ..... Tidy
08-chapter-10\DATA_02.S ..... Unchanged from Chapter 6
08-chapter-10\DATA_03.S ..... Unchanged from Chapter 7
08-chapter-10\DATA_05.S ..... Tidy
08-chapter-10\SYSTM_00.S .... Unchanged from Chapter 2
08-chapter-10\SYSTM_01.S .... Unchanged from Chapter 4
08-chapter-10\SYSTM_02.S .... Unchanged from Chapter 7
08-chapter-10\SYSTM_03.S .... Unchanged from Chapter 8
08-chapter-10\SYSTM_04.S .... Tidy
08-chapter-10\TRNSFRMS.S .... Tidy
-------------------------------------------------------------------------------

TRNSFRMS.PRG
  Spinning cube with joystick control over spinning and angular momentum.
  Cube has ST on each face.

  Very slow at ST standard 8MHz, but looks pretty good with emulation
  accelerated to 32MHz.

  As well as joystick, can use function keys, as follows...

  F1 ... reverse
  F2 ... forward
  F3 ... toggle x shear
  F4 ... toggle y shear
  F5 ... toggle z shear
  F6 ... stop
  F7 ... exit

  Exit makes program unresponsive - if the joystick is then moved, it
  crashes out. This is true of all programs that look at function keys.
  Only tried TOS 1.2 so far.

===============================================================================
08-chapter-10\BSS_02.S ...... Unchanged from Chapter 6
08-chapter-10\BSS_03.S ...... Unchanged from Chapter 7
08_chapter-10\BSS_04.S ...... Unchanged from Chapter 9
08-chapter-10\BSS_05.S ...... Unchanged from Chapter 10
09-chapter-11\BSS_06.S ...... Tidy
08-chapter-10\CORE_00.S ..... Unchanged from Chapter 4
08-chapter-10\CORE_01.S ..... Unchanged from Chapter 5
08-chapter-10\CORE_02.S ..... Unchanged from Chapter 6
08-chapter-10\CORE_03.S ..... Unchanged from Chapter 7
08_chapter-10\CORE_04.S ..... Unchanged from Chapter 9
08-chapter-10\CORE_05.S ..... Unchanged from Chapter 10
09-chapter-11\CORE_06.S ..... Tidy
08-chapter-10\DATA_02.S ..... Unchanged from Chapter 6
08-chapter-10\DATA_03.S ..... Unchanged from Chapter 7
08-chapter-10\DATA_05.S ..... Unchanged from Chapter 10
08-chapter-10\SYSTM_00.S .... Unchanged from Chapter 2
08-chapter-10\SYSTM_01.S .... Unchanged from Chapter 4
08-chapter-10\SYSTM_02.S .... Unchanged from Chapter 7
08-chapter-10\SYSTM_03.S .... Unchanged from Chapter 8
08-chapter-10\SYSTM_04.S .... Unchanged from Chapter 10
09-chapter-11\WRLD_VW.S ..... Tidy
-------------------------------------------------------------------------------

WRLD_VW.PRG
  Same cube as in chapter 10, but now the joystick spins the viewer rather
  than the cube.

  F1 ... reverse
  F2 ... forward
  F3 ... N/A
  F4 ... N/A
  F5 ... N/A
  F6 ... stop
  F7 ... exit

  Exit makes program unresponsive - if the joystick is then moved, it
  crashes out. This is true of all programs that look at function keys.
  Only tried TOS 1.2 so far.

===============================================================================
08-chapter-10\BSS_02.S ...... Unchanged from Chapter 6
08-chapter-10\BSS_03.S ...... Unchanged from Chapter 7
08_chapter-10\BSS_04.S ...... Unchanged from Chapter 9
08-chapter-10\BSS_05.S ...... Unchanged from Chapter 10
09-chapter-11\BSS_06.S ...... Unchanged from Chapter 11
10-chapter-12\BSS_07.S ...... Tidy
08-chapter-10\CORE_00.S ..... Unchanged from Chapter 4
08-chapter-10\CORE_01.S ..... Unchanged from Chapter 5
08-chapter-10\CORE_02.S ..... Unchanged from Chapter 6
08-chapter-10\CORE_03.S ..... Unchanged from Chapter 7
08_chapter-10\CORE_04.S ..... Unchanged from Chapter 9
08-chapter-10\CORE_05.S ..... Unchanged from Chapter 10
09-chapter-11\CORE_06.S ..... Unchanged from Chapter 11
10-chapter-12\CORE_07.S ..... Tidy
10-chapter-12\CORE_08.S ..... Tidy
08-chapter-10\DATA_02.S ..... Unchanged from Chapter 6
08-chapter-10\DATA_03.S ..... Unchanged from Chapter 7
08-chapter-10\DATA_05.S ..... Unchanged from Chapter 10
10-chapter-12\DATA_06.S ..... Tidy
10-chapter-12\DATA_07.S ..... Tidy
10-chapter-12\DATA_08.S ..... Tidy
10-chapter-12\EULR_SCN.S .... Tidy
08-chapter-10\SYSTM_00.S .... Unchanged from Chapter 2
08-chapter-10\SYSTM_01.S .... Unchanged from Chapter 4
08-chapter-10\SYSTM_02.S .... Unchanged from Chapter 7
08-chapter-10\SYSTM_03.S .... Unchanged from Chapter 8
08-chapter-10\SYSTM_04.S .... Unchanged from Chapter 10
10-chapter-12\SYSTM_05.S .... Tidy
10-chapter-12\WRLD_SCN.S .... Tidy
-------------------------------------------------------------------------------

The two examples for this chapter are very similar, allowing a player to
move through a scene. The difference is in the type of viewing transform
used.

  WRLD_SCN.PRG - movements are relative to the viewers current position and
                 orientation.

  EURL_SCN.PRG - movement uses a simpler Euler-angles scheme. Roll isn't
                 supported since this would expose the failings in the
                 Euler-angles scheme - rotations would not be relative to
                 the rolled frame of reference.

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

WRLD_SCN.PRG
  Explore a more complex scene.

  As well as joystick, can use function keys, as follows...

  F1 ... roll left
  F2 ... roll right
  F3 ... reverse speed 2
  F4 ... forward speed 2
  F5 ... forward speed 3
  F6 ... stop
  F7 ... exit

  Exit makes program unresponsive - if the joystick is then moved, it
  crashes out. This is true of all programs that look at function keys.
  Only tried TOS 1.2 so far.

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

EULR_SCN.PRG
  Explore a more complex scene.

  As well as joystick, can use function keys, as follows...

  F1 ... N/A
  F2 ... N/A
  F3 ... reverse speed 2
  F4 ... forward speed 2
  F5 ... forward speed 3
  F6 ... stop
  F7 ... exit

  Exit makes program unresponsive - if the joystick is then moved, it
  crashes out. This is true of all programs that look at function keys.
  Only tried TOS 1.2 so far.

  Previously had a crash when the joystick is moved left/right (normally,
  not after exit). Whitespace-and-comment-only recent fixes seem to have
  resolved this crash.

  On reflection, this makes sense. One change was removing an extra space
  in the ejump_joy table in CORE_08.s (fixed in revision 444). The space
  *looks* irrelevant, but actually means the rest of the table is seen as
  a comment! (note that most comments don't have any explicit comment
  marker - the comment just follows after some statement and some
  whitespace).

  File sizes for EULR_SCN.PRG before and after the fix are...
    Size after fix .... 18,463 bytes
    Size before fix ... 18,448 bytes

  The bad space should only have resulted in three addresses (12 bytes)
  being dropped from the table, so there's another three bytes to account
  for (and I can't explain where they came from so far), but this is
  probably the cause of the crash.

===============================================================================
99-appendix-5\EXAMPLE.S ..... Tidy
99-appendix-5\EXAMPLE2.S .... Not in book - modified version of EXAMPLE.S
-------------------------------------------------------------------------------

EXAMPLE.PRG
  This isn't a complete program - just an example of how to use the LINE-A
  blit function. Because it's not a complete program, it crashes out if you
  try to run it. You can see the blitted result, though.

  Not a full program - crashes or hangs.

EXAMPLE2.PRG
  The same as above, but with an infinite loop added so you can see the
  result. Only seems to work correctly in monochrome mode.

  Infinite loop - no clean exit.

===============================================================================
